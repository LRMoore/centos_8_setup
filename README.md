# Centos_8_setup

## Install some prerequisite utilities

```bash
sudo yum install wget
```

## Install atom text editor

Download the latest rpm from [atom's website](https://atom.io/).

Install missing dependency libXScrnSaver (see
[this issue](https://github.com/atom/atom/issues/13176)):

```bash
sudo pkcon install libXScrnSaver
```

install atom:

```bash
cd Downloads
sudo rpm -i atom.x86_64.rpm
```

## Install anaconda (and python)

Choose and download the python 3.7 version of the linux installer at
[the Anaconda website](https://www.anaconda.com/distribution/#download-section).

Or just do in a terminal (for the current version at time of writing):

```bash
wget https://repo.anaconda.com/archive/Anaconda3-2019.10-Linux-x86_64.sh
```

Run the installer, answering yes to everything:

```bash
bash ~/Downloads/Anaconda3-2019.10-Linux-x86_64.sh
```

Now open a new shell and try `python` to verify it installed correctly. You
should see "Anaconda".

## Preparations to install nvidia drivers

Instructions here based on [this link](https://linuxconfig.org/how-to-install-the-nvidia-drivers-on-centos-8).

Check your graphics hardware (for example, if you have an nvidia card):

```
lspci -vnn | grep -i geforce
```

Note the hardware on the output:

```shell
0a:00.0 3D controller [0302]: NVIDIA Corporation GM107M [GeForce GTX 850M] [10de:1391] (rev a2)
```

Now download the appropriate Linux Game-Ready Driver at the
[NVIDIA page](https://www.nvidia.com/Download/index.aspx?lang=en-us).

For most of the recent (> 800 series) GTX GPUs on linux, this amounts to:

```bash
wget http://uk.download.nvidia.com/XFree86/Linux-x86_64/440.36/NVIDIA-Linux-x86_64-440.36.run
```

Get the prerequisite libraries:

```bash
sudo dnf groupinstall "Development Tools"
sudo dnf install libglvnd-devel elfutils-libelf-devel
```

Permenantly disable the nouveau drivers:

```bash
sudo grub2-editenv - set "$(grub2-editenv - list | grep kernelopts) nouveau.modeset=0"
```

Reboot your system:

```bash
sudo reboot
```

Jump into text mode (ctrl+F3), login and ensure xorg server stopped. this will
require another login:

```bash
sudo systemctl isolate multi-user.target
```

## Install NVIDIA GPU drivers

Now run the NVIDIA installer script.

### Option 1 - secure boot disabled (easier, normal install):

If UEFI secure boot is disabled in BIOS, do:

```bash
cd ~/Downloads
sudo bash NVIDIA-Linux-x86_64-440.36.run
```

Agree to install 32-bit compatibility.

### Option 2 - secure boot enabled (install with signed drivers):

If UEFI secure boot is enabled, you will have to sign the kernel modules by
generating a key pair for the NVIDIA drivers. First follow the instructions at
[askubuntu](https://askubuntu.com/questions/1023036/how-to-install-nvidia-driver-with-secure-boot-enabled).
Be sure to select import MOK and enter the key pair password on reboot.

Then run the installer script, passing the FULL (eg `/home/lmoore/my_key.key`)
key paths in the command line options:

```bash
cd ~/Downloads
sudo bash ./XXXXXX.run -s --module-signing-secret-key=PATH_TO_PRIVATE_KEY --module-signing-public-key=PATH_TO_PUBLIC_KEY
```

Agree to install 32-bit compatibility.

## Verify nvidia installation

To verify the installation, reboot again

```bash
sudo reboot
```

If you encounter a black screen, switch to text mode and try:

```bash
systemctl restart systemd-logind
```

Now check that it is installed correctly:

```bash
lsmod | egrep -i "nvidia|nouveau"
```

Which should show `nvidia_modeset 1`.

I checked the driver being used by the GPU:

```bash
sudo lshw | egrep -A8 -i "nvidia"
```

Which confirmed `configuration: driver=nvidia`

Finally, try the command:

```bash
nvidia-settings
```

If this works, you're good.

### Troubleshooting: nvidia installation on optimus laptops

Accessing `nvidia-settings` failed for me with error message (see following section)

```shell
ERROR: Unable to load info from any available system
```

This problem was related to having an Optimus laptop (one which has some built-in
switching mechanism between the dGPU and the CPU onboard graphics). Check yours.

If you're in the same situation, follow the instructions at [devtalk](https://devtalk.nvidia.com/default/topic/1061210/linux/centos-7-unable-to-run-nvidia-settings/)

To create the appropriate configuration and optimus.desktop files, then reboot.

Check once again:

```bash
nvidia-settings
```

This worked for me.

## Install tensorflow gpu (the easy way)

Time to try to install tensorflow with GPU support. If you got conda and
the nvidia drivers working correctly, try:

```bash
conda install tensorflow-gpu
```

This will take a few iterations to resolve a working environment. When done,
verify that tensorflow is working with GPU support. In a python shell, do:

```python
import tensorflow as tf
tf.test.is_gpu_available()
```

which will return True if you're good to go.

## Install docker

These instructions were found in [this techrepublic article](
https://www.techrepublic.com/article/a-better-way-to-install-docker-on-centos-8/)

Add the docker repository to the yum package manager

```bash
sudo yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo

```

Install docker-ce with the command

```bash
sudo dnf install docker-ce-3:18.09.1-3.el7
```

Start and enable the Docker daemon

```bash
sudo systemctl enable --now docker
```

Add your user to the docker group

```bash
sudo usermod -aG docker $USER
```

Log out and log back in.

To install the containerd.io package, go back to the terminal window and issue the command:

```bash
sudo dnf install https://download.docker.com/linux/centos/7/x86_64/stable/Packages/containerd.io-1.2.6-3.3.el7.x86_64.rpm
```

The secondary benefit of installing containerd.io manually, is that you can now install the latest version of docker-ce, as opposed to installing an older, specific version. Install the latest docker-ce release with the command:

```
sudo dnf install docker-ce
```

start docker

```bash
systemctl start docker
```
### Optional - disable firewall

You might find a lack of internet access inside docker containers when running, e.g.
`apt update`. You can, depending on the underlying problem, fix this by disabling the firewall:

```bash
sudo systemctl disable firewalld
```

and restarting the docker service:

```bash
sudo systemctl stop docker
sudo systemctl start docker
```

## Install docker-compose

Now download docker-compose from the docker github:

```bash
sudo curl -L "https://github.com/docker/compose/releases/download/1.23.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
```

Make it executable:

```bash
sudo chmod +x /usr/local/bin/docker-compose
```

And test that it works:

```bash
docker-compose --version
```

## Install nvidia-docker

We first need to add the repository from NVIDIA and update yum:

```bash
distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.repo | sudo tee /etc/yum.repos.d/nvidia-docker.repo
sudo yum update
```
Accept the GPG keys for nvidia libraries.

Now install the tool:

```bash
sudo yum update
sudo yum install nvidia-docker2
```

(Newer versions exist under the name `nvidia-container-toolkit`) see [this link](https://github.com/NVIDIA/nvidia-docker/issues/1073) but do not support specifying gpu runtimes in docker-compose.)

Restart the docker daemon or reboot.

```bash
sudo service docker restart
```

Now test it by outputting the GPU stats within the container:

```bash
docker run --runtime=nvidia --rm nvidia/cuda:9.0-base nvidia-smi
```

You're good if you see something like:

```shell
+-----------------------------------------------------------------------------+
| NVIDIA-SMI 440.36       Driver Version: 440.36       CUDA Version: 10.2     |
|-------------------------------+----------------------+----------------------+
| GPU  Name        Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |
| Fan  Temp  Perf  Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |
|===============================+======================+======================|
|   0  GeForce GTX 850M    Off  | 00000000:0A:00.0 Off |                  N/A |
| N/A   48C    P8    N/A /  N/A |    371MiB /  4046MiB |      0%      Default |
+-------------------------------+----------------------+----------------------+

+-----------------------------------------------------------------------------+
| Processes:                                                       GPU Memory |
|  GPU       PID   Type   Process name                             Usage      |
|=============================================================================|
+-----------------------------------------------------------------------------+

```

Now in a given `docker-compose.yml` you can specify for a given service
the nvidia runtime as:

```yaml
services:
    my_service:
        image: ubuntu:latest
        runtime: nvidia
        environment:
          - NVIDIA_VISIBLE_DEVICES=all
```

### Configure docker to use nvidia runtime by default

You can make the nvidia runtime used by default by
[updating](https://stackoverflow.com/questions/47465696/how-do-i-specify-nvidia-runtime-from-docker-compose-yml)
`/etc/docker/daemon.json` with first-level key:

```
"default-runtime":"nvidia"
```
